---
- hosts: all
  remote_user: root
  gather_facts: False
  vars_files:
    - vault.yml
    - vars.yml
  tasks:
    - name: Setup server
      include_role:
        name: basics

    - name: Create Sudo user
      include_role:
        name: create-sudo-user
      loop: "{{ __sudo_users }}"
      loop_control:
        loop_var: new_user

    - name: Lock UFW
      include_role:
        name: lock-ufw
    
    - name: Lock SSH
      include_role:
        name: lock-ssh
      vars:
        ssh_users: "{{ __sudo_users | join(' ', 'name') }} {{ __deployer_user.name }}"


- hosts: app
  remote_user: "{{ __sudo_users[0].name }}"
  become: True
  gather_facts: False
  vars_files:
    - vault.yml
    - vars.yml
  tasks:
    - name: Create Deployer user
      include_role:
        name: create-deployer-user
      vars:
        project_location: "{{ __project_location }}"
        deployer_user: "{{ __deployer_user }}"
    
    - name: Setup Nginx
      include_role:
        name: nginx
      vars:
        deployer_user: "{{ __deployer_user }}"
        project_location: "{{ __project_location }}"
        site_template_name: "{{ __site_template_name }}"
        site_is_laravel: "{{ __site_is_laravel }}"
        domain_names: "{{ __domain_names }}"
    - name: Setup PHP
      include_role:
        name: php

    - name: Setup PHP-FPM
      include_role:
        name: configure-phpfpm-user
      vars:
        deployer_user: "{{ __deployer_user }}"
        site_php_workers: "{{ __site_php_workers }}"

    - name: Setup PHP Composer
      include_role:
        name: install-php-composer
      when: __site_is_laravel

    - name: Add db private ip in app hosts file
      lineinfile:
        path: /etc/hosts
        state: present
        regexp: '\d+\.\d+\.\d+\.\d+\s+db'
        line: "{{ hostvars['db']['private_ip'] }}  db"


- hosts: db
  remote_user: "{{ __sudo_users[0].name }}"
  become: True
  gather_facts: False
  vars_files:
    - vault.yml
    - vars.yml
  tasks:
    - name: Setup MySQL
      include_role:
        name: mysql
      vars:
        mysql_root_pass: "{{ __mysql_root_pass }}"
        database: "{{ __database }}"
        private_ip: "{{ hostvars['db']['private_ip'] }}"
    
    - name: Add app private ip in db hosts file
      lineinfile:
        path: /etc/hosts
        state: present
        regexp: '\d+\.\d+\.\d+\.\d+\s+app'
        line: "{{ hostvars['app']['private_ip'] }}  app"

    - name: Add UFW rule to allow from app
      ufw:
        rule: allow
        from_ip: "{{ hostvars['app']['private_ip'] }}"
    
    - name: Restart ufw
      service:
        name: ufw
        state: restarted