# Steps for Access

## Both Servers Users

* sudo user is `username`
* deployer user is `username`

## App Server

* IP: `xxx.xxx.xxx.xxx`
* Deployment Folder `/home/username/app`
  * That's where you should clone your project or deploy it whatever the way. Please, try to stick to this folder (not only the name as it has permissions set and all). If you need to delete/recreate it, do so, then, notify me to set proper permissions later.

## MySQL Server

*To access it from the `app` server, the host will be `db`*

* IP: `xxx.xxx.xxx.xxx`
* database: `database_name`

### Root User

To access mysql as root, you need to become root "sudo su" then you can run "mysql" and voila, you're in. _Note that root access is allowed only for localhost._

### App User

* user: `user_name`
* password: ****
